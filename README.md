#  Movie List

## How to Setup

**Step 1:** git clone this repo:

**Step 2:** cd to the cloned repo:

**Step 3:** Install the Application with `npm install` or `yarn`


## How to Run App

1. cd to the repo
2. `npm start` or `yarn start`