import React from 'react'
import { Image } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
import NetInfo from '@react-native-community/netinfo'
import { AppLoading } from 'expo'
import * as Font from 'expo-font'
import { Ionicons } from '@expo/vector-icons'
import {
  Container,
  Header,
  Content,
  Card,
  CardItem,
  Title,
  Text,
  Left,
  Body,
  Right
} from 'native-base'


const App = () => {

  const [isReady, setIsReady] = React.useState(false)

  React.useEffect(() => {
    (async () => {
      await Font.loadAsync({
        Roboto: require('native-base/Fonts/Roboto.ttf'),
        Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
        ...Ionicons.font,
      })
      setIsReady(true)
    })()
  }, [])

  return !isReady ? <AppLoading /> : <HomeScreen />
}


const HomeScreen = () => {

  const [movieData, setMovieData] = React.useState([])

  const fetchMovie = () => {
    const url = 'http://www.omdbapi.com/?apiKey=9e07ef1&t='
    const movieList = ['One Piece', 'Warcraft', 'Thor', 'Final Fantasy']

    const response = movieList.map(movie => {
      return new Promise((resolve, reject) => {
        fetch(url + movie)
          .then(response => response.json())
          .then(data => resolve(data))
          .catch(err => reject(err))
      })
    })

    Promise.all(response).then(movieData => {
      (async () => {
        try {
          await AsyncStorage.setItem('@movie_data', JSON.stringify(movieData))
        } catch (err) {
          // saving error
          console.log('Asyncstorage error', err)
        }
      })()
      setMovieData(movieData)
    })
  }

  const checkConnection = () => {
    NetInfo.isConnected.fetch().then(isConnected => {
      if (!isConnected) {
        (async () => {
          try {
            const value = await AsyncStorage.getItem('@movie_data')
            if(value !== null) {
              setMovieData(JSON.parse(value))
            }
          } catch(err) {
            // error reading value
            console.log('Asyncstorage error', err)
          }
        })()
      }
      console.log('First, is ' + (isConnected ? 'online' : 'offline'))
    })
    function handleFirstConnectivityChange(isConnected) {
      console.log('Then, is ' + (isConnected ? 'online' : 'offline'))
      if (!isConnected) alert('Offile')
      NetInfo.isConnected.removeEventListener(
        'change',
        handleFirstConnectivityChange
      )
    }
    NetInfo.isConnected.addEventListener(
      'change',
      handleFirstConnectivityChange
    )
  }

  const componentDidMount = () => {
    fetchMovie()
    checkConnection()
  }

  // Screen Lifecycle
  React.useEffect(componentDidMount, [])

  return (
    <Container>
      <Header style={{ backgroundColor: '#333' }}>
        <Left />
        <Body>
          <Title>Movie List</Title>
        </Body>
        <Right />
      </Header>
      <Content>
        {movieData.map((movie, index) => <MovieCard key={index} data={movie} />)}
      </Content>
    </Container>
  )
}


const MovieCard = ({ data }) => {

  return (
    <Card>
      <CardItem>
        <Left>
          <Body>
            <Text>{data.Title}</Text>
            <Text note>{data.Genre}</Text>
          </Body>
        </Left>
      </CardItem>
      <CardItem cardBody>
        <Image source={{ uri: data.Poster }} style={{ height: 200, width: null, flex: 1 }} />
      </CardItem>
      <CardItem>
        <Left>
          <Text style={{ fontSize: 13, fontWeight: 'bold', color: '#757575' }}>{data.imdbRating}/10</Text>
        </Left>
        <Body />
        <Right>
          <Text>Release {data.Year}</Text>
        </Right>
      </CardItem>
    </Card>
  )
}


export default App
